<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */





class facebooktimeline_PortletDefinition_timeline implements portlet_PortletDefinitionInterface
{

    private $id;
    private $name;
    private $description;

    public function __construct()
    {
        $this->id = 'facebook_timeline';
        $this->name = facebooktimeline_translate('Facebook timeline');
        $this->description = '';
        $this->addon = bab_getAddonInfosInstance('facebooktimeline');
    }

    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function getDescription()
    {
        return $this->description;
    }


    public function getPortlet()
    {
        require_once dirname(__FILE__).'/timelineportlet.class.php';
        return new facebooktimeline_Portlet_timeline();
    }

    /**
     * Returns the widget rich icon URL.
     *
     *
     * @return string
     */
    public function getRichIcon()
    {
        return $this->addon->getImagesPath() . 'icon.png';
    }


    /**
     * Returns the widget icon URL.
     *
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->addon->getImagesPath() . 'icon.png';
    }

    /**
     * Get thumbnail URL
     * max 120x60
     */
    public function getThumbnail()
    {
        return null;
    }

    public function getConfigurationActions()
    {
        return array();
    }

    public function getPreferenceFields()
    {
        return array(
            array(
                'label' => facebooktimeline_translate('Facebook page url'),
                'type' => 'string',
                'name' => 'url'
            ),
            array(
                'label' => facebooktimeline_translate('Display friends photos'),
                'type' => 'boolean',
                'name' => 'facepile'
            ),
        	array(
        		'label' => facebooktimeline_translate('Height'),
        		'type' => 'string',
        		'name' => 'height'
        	)
        );
    }
}
