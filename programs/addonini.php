; <?php/*
[general]
name							="facebooktimeline"
version							="0.0.4"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Facebook Timeline"
description.fr					="Module permettant d'afficher un compte facebook dans un portlet"
delete							=1
addon_type						="EXTENSION"
ov_version						="8.3.0"
php_version						="5.2.0"
addon_access_control			=0
author							="Cantico"
icon							="icon.png"
tags							="extension,portlet,recommend"

[addons]

portlets="0.18"

;*/?>