<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */




class facebooktimeline_Portlet_timeline extends Widget_Item implements portlet_PortletInterface
{

    private $options = array();





    /**
     * @param Widget_Canvas	$canvas
     * @ignore
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = bab_Widgets();

        if (!isset($this->options['url'])) {
            return '';
        }


        $url = $this->options['url'];
        
        $facepile = 'true';
        if (isset($this->options['facepile']) && !(bool) $this->options['facepile']) {
            $facepile = 'false';
        }
        
        if (isset($this->options['height'])) {
            $height = $this->options['height'];
        } else {
            $height = 900;
        }


        return '
          <style type="text/css" scoped>
    			#fb-root {
                  display: none;
                }
                
                .fb_iframe_widget,
                .fb_iframe_widget span,
                .fb_iframe_widget span iframe[style] {
                  width: 100% !important;
                }
    	  </style>
            
          <div class="fb-page" data-href="'.bab_toHtml($url).'" data-height="'.bab_tohtml($height).'" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="'.$facepile.'" data-tabs="timeline">
            <div class="fb-xfbml-parse-ignore">
                <blockquote cite="Facebook page">
                    <a href="'.bab_toHtml($url).'">Facebook page</a>
                </blockquote>
            </div>
          </div>
                        
          <div id="fb-root"></div>
     
          <script type="text/javascript">(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.5";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, \'script\', \'facebook-jssdk\'));
          </script>  
		';
    }




    public function getName()
    {
        return get_class($this);
    }

    public function getPortletDefinition()
    {
        return new facebooktimeline_PortletDefinition_timeline();
    }

    /**
     * Get the title defined in preferences or a default title for the widget frame
     * @return string
     */
    public function getPreferenceTitle()
    {
        if (!empty($this->options['_blockTitle']))
        {
            return $this->options['_blockTitle'];
        }

        return '';
    }

    /**
     * receive current user configuration from portlet API
     */
    public function setPreferences(array $configuration)
    {
        $this->options = $configuration;
    }

    public function setPreference($name, $value)
    {
        $this->options[$name] = $value;
    }

    public function setPortletId($id)
    {

    }
}