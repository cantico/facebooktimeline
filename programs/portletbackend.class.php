<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


class Func_PortletBackend_FacebookTimeline extends Func_PortletBackend
{
    public function getDescription()
    {
        require_once dirname(__FILE__).'/functions.php';
        return facebooktimeline_translate("Facebook timeline");
    }
    
    
    public function select($category = null)
    {
        if (isset($category) && $category !== 'facebook')
        {
            return array();
        }
    
        $portlets = array(
            'facebook_timeline' => $this->Portlet_timeline()
        );
    
        return $portlets;
    }
    
    
    /**
     * Get portlet definition instance
     * @param	string	$portletId			portlet definition ID
     *
     * @return portlet_PortletDefinitionInterface
     */
    public function getPortletDefinition($portletId)
    {
        $name = str_replace('facebook_', '', $portletId);
    
        switch($name)
        {
            case 'timeline':
                return $this->Portlet_timeline();
        }
    
        return null;
    }
    
    
    public function Portlet_timeline()
    {
        require_once dirname(__FILE__).'/functions.php';
        require_once dirname(__FILE__).'/timelineportletdefinition.class.php';
        return new facebooktimeline_PortletDefinition_timeline();
    }

    
    
    /**
     * get a list of categories supported by the backend
     * @return Array
     */
    public function getCategories()
    {
        require_once dirname(__FILE__).'/functions.php';
    
        return array(
            'facebook' => facebooktimeline_translate('Facebook')
        );
    }
}

















